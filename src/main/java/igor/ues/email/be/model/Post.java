package igor.ues.email.be.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "post")
public class Post {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id", unique = true, nullable = false)
    private Integer id;

	@Column(name = "title", unique = false, nullable = false)
	private String title;
	
	@Column(name = "text", unique = false, nullable = false)
	private String text;
	
	@Column(name = "creation_date", unique = false, nullable = true)
	private LocalDate creationDate;
	
	@Column(name = "image_path", unique = false, nullable = true)
	private String imagePath;
	
	@ManyToOne()
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	private User user;
	
	@ManyToOne()
	@JoinColumn(name = "community_id", referencedColumnName = "community_id", nullable = false)
	private Community community;
	
	@ManyToOne()
	@JoinColumn(name = "flair_id", referencedColumnName = "community_id", nullable = true)
	private Flair flair;
	
	@OneToMany(mappedBy = "post", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Comment> comments;
	
	@OneToMany(mappedBy = "post", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Report> reports;
	
}
