package igor.ues.email.be.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Moderator")
public class Moderator extends User {

}
