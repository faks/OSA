package igor.ues.email.be.model.enums;

public enum ReactionType {
	
	UPVOTE,
	DOWNVOTE
}
